--[[
    Medal Class
]]

Medal = Class{}

local BRONZE_IMAGE = love.graphics.newImage('bronze.png')
local SILVER_IMAGE = love.graphics.newImage('silver.png')
local GOLD_IMAGE = love.graphics.newImage('gold.png')

local image

function Medal:award(score)
	if score >= 15 then
		self.image = GOLD_IMAGE
	elseif score >= 10 then
		self.image = SILVER_IMAGE
	elseif score >= 5 then
		self.image = BRONZE_IMAGE
	end
	
	if self.image then
		return true
	end

	return false
end

function Medal:render()
	if self.image then
		local width = self.image:getWidth()
		local height = self.image:getHeight()
		
		love.graphics.draw(self.image, VIRTUAL_WIDTH / 2 - width / 4, VIRTUAL_HEIGHT / 2 - height / 4, 0, 0.5, 0.5)
	end
end