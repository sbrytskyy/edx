--[[
    GD50
    Breakout Remake
    Assignment 2

    -- Powerup Class --
]]

Powerup = Class{}

function Powerup:init(x, y)
    -- used for coloring and score calculation
    self.type = 1
    
    self.x = x
    self.y = y
    self.width = 16
    self.height = 16
    
    self.dy = math.random(50, 60)
end

--[[
    Expects an argument with a paddle
    and returns true if the bounding boxes of this and the argument overlap.
]]
function Powerup:collides(target)
    -- first, check to see if the left edge of either is farther to the right
    -- than the right edge of the other
    if self.x > target.x + target.width or target.x > self.x + self.width then
        return false
    end

    -- then check to see if the bottom edge of either is higher than the top
    -- edge of the other
    if self.y > target.y + target.height or target.y > self.y + self.height then
        return false
    end 

    -- if the above aren't true, they're overlapping
    return true
end

function Powerup:isKey()
    return self.type == 10
end

function Powerup:isBall()
    return self.type >= 7 and self.type <= 9
end


function Powerup:update(dt)
    self.y = self.y + self.dy * dt
end

function Powerup:render()
    love.graphics.draw(gTextures['main'], gFrames['powerups'][self.type], self.x, self.y)
end
