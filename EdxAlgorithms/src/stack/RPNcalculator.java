package stack;

import java.util.Stack;

public class RPNcalculator {

	/*
	 * Implement a simple RPN calculator
	 * with an Stack
	 * this is a possible solution, others are
	 * also possible
	 */
	public static int calculate(String ops[]) {
		int result = 0;
		int x, y;
		String operators = new String("+-/*");
		Stack<Integer> st = new Stack<>();

		for (String op : ops) {
			if (operators.contains(op)) {
				x = st.pop();
				y = st.pop();
				if (op.equals("+")) {
					st.push(y + x);
				} else if (op.equals("-")) {
					st.push(y - x);
				} else if (op.equals("*")) {
					st.push(y * x);
				} else if (op.equals("/")) {
					st.push(y / x);
				}
			} else {
				st.push(Integer.valueOf(op));
			}
		}

		if (!st.empty()) {
			result= st.pop();
		}
		return result;
	}

	public static void main(String args[]) {
		/*
		 * The main program should print
		    5 3 + = 8
		    5 3 - = 2
		    2 1 12 3 / - + = -1
		    3 2 * 11 - = -5
		*/

		String[] argu = new String[] { "5", "3", "+" };
		int result = calculate(argu);
		System.out.println("5" + " 3" + " +" + " = " + result);
		result = calculate(new String[] { "5", "3", "-" });
		System.out.println("5" + " 3" + " -" + " = " + result);
		result = calculate(new String[] { "2", "1", "12", "3", "/", "-", "+" });
		System.out.println("2" + " 1" + " 12" + " 3" + " /" + " -" + " +" + " = " + result);
		result = calculate(new String[] { "3", "2", "*", "11", "-" });
		System.out.println("3" + " 2" + " *" + " 11" + " -" + " = " + result);

	}
}
