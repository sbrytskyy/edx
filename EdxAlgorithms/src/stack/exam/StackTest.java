package stack.exam;

import java.util.Stack;

public class StackTest {

	public static void main(String[] args) {
		StackTest t = new StackTest();
		t.test();
	}

	private void test() {
		Stack<Integer> si = new Stack<>();
		for (int i = 0; i < 10; i++) {
			si.push(i);
		}
		print(si);
	}

	void print(Stack stack) {
		int element;
		if (!stack.isEmpty()) {
			element = (int) stack.pop();
			print(stack);
			System.out.println(element + "-");
		}
	}
}
