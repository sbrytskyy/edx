package stack;

import java.util.Stack;

public class StackTest {
	
	void test(Stack<Integer> s1) {
		Stack<String> s2 = new Stack<String>();
		int a = 0;
		while (!s1.isEmpty()) {
			int b = s1.pop();
			if (b > a) {
				s2.push("*");
			} else if (b < a) {
				s2.push("-");
			}
			a = b;
		}
		while (!s2.isEmpty()) {
			System.out.println(s2.pop());
		}
	}
	
	public static void main(String[] args) {
		StackTest t = new StackTest();
		Stack<Integer> s1 = new Stack<>();
		s1.push(1);
		s1.push(3);
		s1.push(5);
		s1.push(6);
		s1.push(2);
		s1.push(2);
		t.test(s1);
	}
}
