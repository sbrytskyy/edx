package dequeue.linkedlist;

/**
 * Class that implements a Deque with a Double Linked List.
 *
 * @author DIT-UC3M
 *
 */
public class DLDqueue<E> implements Dqueue<E> {
	// Attributes

	DNode<E> head;
	DNode<E> tail;

	private int size;

	public DLDqueue() {
		head = new DNode<>();
		tail = new DNode<>();
		tail.setPrev(head);
		head.setNext(tail);
	}

	public void insertFirst(E obj) {
		DNode<E> next = head.getNext();
		DNode<E> node = new DNode<>(obj, next, head);
		head.setNext(node);
		next.setPrev(node);

		size++;
	}

	public void insertLast(E obj) {
		DNode<E> prev = tail.getPrev();
		DNode<E> node = new DNode<E>(obj, tail, prev);
		prev.setNext(node);
		tail.setPrev(node);
		
		size++;
	}

	public E removeFirst() {
		if (size == 0)
			return null;

		E result = head.getNext().getVal();

		head = head.getNext();
		head.setPrev(null);
		
		size--;

		return result;
	}

	public E removeLast() {
		if (size == 0)
			return null;

		E result = tail.getPrev().getVal();

		tail = tail.getPrev();
		tail.setNext(null);

		size--;
		
		return result;
	}

	public int size() {
		return size;
	}

	public String toString() {
		String s = "";

		DNode<E> node = head.getNext();
		while (node != tail) {
			s += node.getVal();
			if (node.getNext() != tail) {
				s += ", ";
			}
			node = node.getNext();
		}
		return s;
	}

	public static void main(String[] args) {
		DLDqueue<Integer> dq = new DLDqueue<>();
		for (int i = 0; i < 10; i++) {
			if (i % 2 == 0) {
				dq.insertFirst(i);
			} else {
				dq.insertLast(i);
			}
			
			System.out.println(i + " : " + dq);
		}
		
		System.out.println("dq: " + dq);
	}

}
