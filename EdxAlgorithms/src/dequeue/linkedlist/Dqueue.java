package dequeue.linkedlist;

/**
 * Interface for a Deque.
 *
 * @author DIT-UC3M
 *
 */
public interface Dqueue<E> {
	void insertFirst(E obj);

	void insertLast(E obj);

	E removeFirst();

	E removeLast();

	int size();
}