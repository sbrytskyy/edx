package dequeue.linkedlist;

/**
 * An Implementation of a Double Linked List Node
 *
 * @author DIT-UC3M
 *
 */
public class DNode<E> {

	DNode<E> next, prev;
	E val;

	public DNode() {

		this.next = null;
		this.prev = null;
		this.val = null;
	}

	public DNode(E val, DNode<E> next, DNode<E> prev) {

		this.next = next;
		this.prev = prev;
		this.val = val;
	}

	public DNode<E> getNext() {
		return next;
	}

	public void setNext(DNode<E> next) {
		this.next = next;
	}

	public DNode<E> getPrev() {
		return prev;
	}

	public void setPrev(DNode<E> prev) {
		this.prev = prev;
	}

	public E getVal() {
		return val;
	}

	public void setVal(E val) {
		this.val = val;
	}

}
