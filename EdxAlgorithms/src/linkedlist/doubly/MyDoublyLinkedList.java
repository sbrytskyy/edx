package linkedlist.doubly;

public class MyDoublyLinkedList<E extends Comparable<E>> {
	private Node<E> head;
	private Node<E> tail;

	public MyDoublyLinkedList() {
		this.head = null;
		this.tail = null;
	}

	/*
	 * Insertion at the beginning
	 */
	public void insert(E info) {
		Node<E> newNode = new Node<E>(info);
		newNode.setNext(head);
		if (head != null) {
			head.setPrev(newNode);
		}
		head = newNode;
		if (tail == null) {
			tail = newNode;
		}
	}

	/*
	 * Insertion at the end
	 */
	public void insertEnd(E info) {
		Node<E> newNode = new Node<E>(info);
		if (tail == null) {
			head = newNode;
			tail = newNode;
		} else {
			tail.setNext(newNode);
			newNode.setPrev(tail);
			tail = newNode;
		}
	}

	/*
	 * Extraction of the first node
	 */
	public E extract() {
		E data = null;
		if (head != null) {
			data = head.getInfo();

			if (head.getNext() != null) {
				head = head.getNext();
				head.setPrev(null);
			} else {
				head = null;
				tail = null;
			}
		}
		return data;
	}

	/*
	 * Extraction of the last node
	 */
	public E extractEnd() {
		E data = null;
		if (tail != null) {
			data = tail.getInfo();
			if (head == tail) {
				head = null;
				tail = null;
			} else {
				if (tail.getPrev() != null) {
					tail = tail.getPrev();
					tail.setNext(null);
				} else {
					head = null;
					tail = null;
				}
			}
		}
		return data;
	}
	/*
	 * Delete all nodes with info equal to value returns number of deleted nodes
	 */

	public int deleteAll(E info) {
		int deleted = 0;

		if (head != null) {
			Node<E> node = head;
			while (node != null) {
				if (node.getInfo().compareTo(info) == 0) {
					deleted++;
					
					if (node.getNext() != null) {
						node.getNext().setPrev(node.getPrev());
					} else {
						if (node.getPrev() != null) {
							tail = node.getPrev();
							tail.setNext(null);
						} else {
							tail = null;
							head = null;
						}
					}
					if (node.getPrev() != null) {
						node.getPrev().setNext(node.getNext());
					} else {
						if (node.getNext() != null) {
							head = node.getNext();
							head.setPrev(null);
						} else {
							tail = null;
							head = null;
						}
					}
				}
				node = node.getNext();
			}
		}

		return deleted;
	}

	/*
	 * Print all list forward
	 */
	public void print() {
		Node<E> current = head;

		while (current != null) {
			System.out.print(current.getInfo() + " ");
			current = current.getNext();
		}
		System.out.println();
	}

	/*
	 * Print all list backwards
	 */
	public void printBackwards() {
		Node<E> current = tail;

		while (current != null) {
			System.out.print(current.getInfo() + " ");
			current = current.getPrev();
		}
		System.out.println();
	}
}
