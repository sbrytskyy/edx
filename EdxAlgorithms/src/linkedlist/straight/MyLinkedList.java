package linkedlist.straight;
public class MyLinkedList<E extends Comparable<E>>{
    // Notice that stating E extends Comparable<E> is needed, as we should use
    // compareTo for inserting in a sorted way
	private Node<E> first;

	public MyLinkedList() {
		this.first = null;
	}

	/*
	 * Insertion at the beginning
	 */
	public void insert(E info) {
		Node<E> newNode = new Node<E>(info);
		if (first == null) {
			first = newNode;
			return;
		}
		
		if (first.getInfo().compareTo(info) > 0) {
			newNode.setNext(first);
			first = newNode;
			return;
		}
		
		Node<E> node = first;
		while (node.getNext() != null && node.getNext().getInfo().compareTo(info) < 0) {
			node = node.getNext();
		}
		newNode.setNext(node.getNext());
		node.setNext(newNode);
	}

	/*
	 * Insertion at the end Implement this method
	 */
	public void insertEnd(E info) {
		Node<E> newNode = new Node<E>(info);
		if (first == null) {
			first = newNode;
			return;
		}
		Node<E> node = first;
		while (node.getNext() != null) {
			node = node.getNext();
		}
		node.setNext(newNode);
	}

	/*
	 * Extraction of the first node
	 */
	public E extract() {
		E data = null;
		if (first != null) {
			data = first.getInfo();
			first = first.getNext();
		}
		return data;
	}

	/*
	 * Print all list
	 */
	public void print() {
		Node<E> current = first;

		while (current != null) {
			System.out.print(current.getInfo() + " ");
			current = current.getNext();
		}
		System.out.println();
	}

	public void method() {
		if (first != null && first.getNext() != null) {
			Node<E> aux = first;
			while (aux.getNext().getNext() != null) {
				aux = aux.getNext();
			}
			System.out.println(aux.getInfo());
		}
	}
}
