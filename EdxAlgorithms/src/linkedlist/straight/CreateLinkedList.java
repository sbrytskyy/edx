package linkedlist.straight;
/*
 * Exercise for Creation of a Linked List and printing
 */
public class CreateLinkedList {

	public static void main(String args[]) {
		// Create a linked list using MyLinkedList<Integer>
		MyLinkedList<Integer> l = new MyLinkedList<>();

		//int[] ai = { 3, 1, 2, 5, 4 };
		// Insert the first 10 ints at the beginning
		for (int i = 0; i < 100; i++) {
			l.insert((int)(1000*Math.random()));
			//l.insert(ai[i]);
		}
		// Print the whole list
		l.print();
		l.method();

		// Create another linked list using MyLinkedList<Integer>
		MyLinkedList<Integer> l2 = new MyLinkedList<>();

		// Insert 10 ints at the end
		for (int i = 0; i < 10; i++) {
			l2.insertEnd(i);
		}
		// Print the whole second list
		l2.print();
		l2.method();
	}
}
