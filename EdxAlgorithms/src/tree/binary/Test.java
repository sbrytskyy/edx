package tree.binary;

/*
 * Binary Tree interface
 */
public class Test {
	public static void main(String args[]) {
		try {
			// First, create nodes. For instance, as follows:
			BTree<String> colorGreen = new DummyTree<String>("color=GREEN");
			BTree<String> sizeBig = new DummyTree<String>("size=BIG");
			BTree<String> colorYellow = new DummyTree<String>("color=YELLOW");
			// ...
			BTree<String> waterlemon = new DummyTree<String>("color=WATERLEMON");

			// Then connect nodes. For instance, as follows:
			colorGreen.insert(sizeBig, BTree.LEFT);
			colorGreen.insert(colorYellow, BTree.RIGHT);
			// ...
			sizeBig.insert(waterlemon, BTree.LEFT);
			
			// Print the tree and its size and height

		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
	}
}