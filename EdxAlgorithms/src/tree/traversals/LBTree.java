package tree.traversals;

public class LBTree<E> implements BTree<E> {

	// Attribute that represents the root node (recursive definition)
	private LBNode<E> root;

	/*
	 * Constructor of an empty tree
	 */
	public LBTree() {
		root = null;
	}

	/*
	 * Constructor
	 */
	public LBTree(E info) {
		root = new LBNode<E>(info, new LBTree<E>(), new LBTree<E>());
	}

	/*
	 * Checks if the tree is empty
	 */
	public boolean isEmpty() {
		return (root == null);
	}

	/*
	 * Returns the information stored in the node
	 */
	public E getInfo() throws BTreeException {
		if (isEmpty()) {
			throw new BTreeException("Empty trees do not have info");
		}
		return root.getInfo();
	}

	/*
	 * Returns left and right subtrees
	 */
	public BTree<E> getLeft() throws BTreeException {
		if (isEmpty()) {
			throw new BTreeException("Empty trees do not have a left child");
		}
		return root.getLeft();
	}

	public BTree<E> getRight() throws BTreeException {
		if (isEmpty()) {
			throw new BTreeException("Empty trees do not have a right child");
		}
		return root.getRight();
	}

	/*
	 * Inserts a given tree as a subtree in the given side
	 */
	public void insert(BTree<E> tree, int side) throws BTreeException {
		if (isEmpty()) {
			throw new BTreeException("Empty trees do not have a left or right child");
		}
		if (side == LEFT) {
			if (!root.getLeft().isEmpty()) {
				throw new BTreeException("Left subtree is not empty");
			}
			root.setLeft(tree);
		} else {
			if (!root.getRight().isEmpty()) {
				throw new BTreeException("Right subtree is not empty");
			}
			root.setRight(tree);
		}
	}

	/*
	 * Tree traversals
	 */
	public String toStringPreOrder() {
		if (isEmpty()) {
			return "";
		} else {
			return root.getInfo().toString() + " " +
					root.getLeft().toStringPreOrder() + " " +
					root.getRight().toStringPreOrder();
		}
	}

	public String toStringInOrder() {
		if (isEmpty()) {
			return "";
		} else {
			return root.getLeft().toStringInOrder() + " " +
					root.getInfo().toString() + " " +
					root.getRight().toStringInOrder();
		}
	}

	public String toStringPostOrder() {
		if (isEmpty()) {
			return "";
		} else {
			return root.getLeft().toStringPostOrder() + " " +
					root.getRight().toStringPostOrder() + " " +
					root.getInfo().toString();
		}
	}

	/*
	 * Tree properties
	 */
	public int size() {
		if (isEmpty()) return 0;
//		System.out.println("Returns the size of [" + root.getInfo() + "] tree");
		int size = 1;
		if (root.getLeft() != null) {
			size += root.getLeft().size();
		}
		if (root.getRight() != null) {
			size += root.getRight().size();
		}
		return size;
	}

	public int height() {
		if (isEmpty()) return -1;
//		System.out.println("Returns the height of [" + root.getInfo() + "] tree");
		int height = 1;
		int heightLeft = 0;
		int heightRight = 0;
		if (root.getLeft() != null) {
			heightLeft = root.getLeft().height();
		}
		if (root.getRight() != null) {
			heightRight = root.getRight().height();
		}
		return height + Math.max(heightLeft, heightRight);
	}

}