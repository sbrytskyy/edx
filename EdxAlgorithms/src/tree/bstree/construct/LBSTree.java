package tree.bstree.construct;

public class LBSTree<E> implements BSTree<E> {

	/*
	 * Attribute: the node
	 */
	private LBSNode<E> root;

	/*
	 * Constructors
	 */
	public LBSTree(Comparable key, E info) {
		root = new LBSNode<E>(key, info, new LBSTree<E>(), new LBSTree<E>());
	}

	public LBSTree() {
		root = null;
	}

	/* 
	 * Checks if the tree is empty
	 */
	public boolean isEmpty() {
		return (root == null);
	}

	/*
	 * Returns the search key of the information stored in the tree
	 */
	public Comparable getKey() {
		if (isEmpty()) {
			return null;
		} else {
			return root.getKey();
		}
	}

	/*
	 * Returns the information stored in the tree
	 */
	public E getInfo() {
		return root.getInfo();
	}

	/* 
	 * Returns the left and right subtrees
	 */
	public BSTree<E> getLeft() {
		return root.getLeft();
	}

	public BSTree<E> getRight() {
		return root.getRight();
	}

	/*
	 * Inserts information into the tree. If key already exists, the information is overwritten
	 */
	public void insert(Comparable key, E info) {
		if (isEmpty()) {
			root = new LBSNode<E>(key, info, new LBSTree<E>(), new LBSTree<E>());
		} else {
			if (root.getKey().compareTo(key) > 0) {
				getLeft().insert(key, info);
			} else if (root.getKey().compareTo(key) < 0) {
				getRight().insert(key, info);
			} else {
				root.setInfo(info);
			}
		}
	}

	/*
	 * Searches for information in the tree
	 */
	public E search(Comparable key) {
		if (isEmpty())
			return null;
		int result = root.getKey().compareTo(key);
		System.out.println("key: " + key + ", root.getKey(): " + root.getKey() + ", compare: " + result);
		if (result == 0) {
			return root.getInfo();
		} else if (result > 0) {
			return root.getLeft().search(key);
		} else {
			return root.getRight().search(key);
		}
	}

	/*
	 * Searches and extracts information (dummy implementation)
	 */
	public E extract(Comparable key) {
		return null;
	}

	/*
	 * Tree traversals (dummy implementation)
	 */
	public String toStringPreOrder() {
		if (isEmpty())
			return "";

		return root.getInfo() + " " + root.getLeft().toStringPreOrder() + root.getRight().toStringPreOrder();
	}

	public String toStringInOrder() {
		if (isEmpty())
			return "";

		return root.getLeft().toStringInOrder() + root.getInfo() + " " + root.getRight().toStringInOrder();
	}

	public String toStringPostOrder() {
		if (isEmpty())
			return "";

		return  root.getLeft().toStringPostOrder() + root.getRight().toStringPostOrder() + root.getInfo() + " ";
	}

	/*
	 * Returns size of the tree (the number of pieces of information) (dummy implementation)
	 */
	public int size() {
		if (isEmpty()) return 0;
		return 1 + root.getLeft().size() + root.getRight().size();
	}

}