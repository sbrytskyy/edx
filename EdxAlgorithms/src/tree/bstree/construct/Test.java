package tree.bstree.construct;

/*
 * Implementation of Binary Search Trees
 */
public class Test {

	public static void main(String args[]) {
		// FIFA World Rankings http://en.wikipedia.org/wiki/FIFA_World_Rankings (9 Feb 2017)

		BSTree<Integer> tree = new LBSTree();
		tree.insert("Brazil", 1672);
		tree.insert("Argentina", 1603);
		tree.insert("Germany", 1464);
		tree.insert("Chile", 1411);
		tree.insert("Colombia", 1348);
		tree.insert("France", 1294);
		tree.insert("Belgium", 1281);
		tree.insert("Portugal", 1259);
		tree.insert("Switzerland", 1212);
		tree.insert("Spain", 1204);
		tree.insert("Poland", 1183);
		tree.insert("Italy", 1165);
		tree.insert("Wales", 1119);
		tree.insert("England", 1103);
		tree.insert("Uruguay", 1097);
		tree.insert("Mexico", 1076);
		tree.insert("Peru", 1044);
		tree.insert("Croatia", 1016);
		tree.insert("Egypt", 910);
		tree.insert("Costa Rica", 902);
		// ... (to complete)

		System.out.println("Spain: " + tree.search("Spain"));
		System.out.println("Italy: " + tree.search("Italy"));

		System.out.println("PREORDER: " + tree.toStringPreOrder());
		System.out.println("INORDER: " + tree.toStringInOrder());
		System.out.println("POSTORDER: " + tree.toStringPostOrder());

		System.out.println("SIZE= " + tree.size());
	}

}