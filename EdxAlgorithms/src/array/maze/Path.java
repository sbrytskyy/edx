package array.maze;

/**
 * Class MazePath represents a path through a maze composed of a doubly-linked
 * list of MazeSteps (positions)
 * 
 * @author Raquel M. Crespo-Garcia <rcrespo@it.uc3m.es>
 */
public class Path {

	/** First position in the path */
	private PathStep first;

	/** Last position in the path */
	private PathStep last;

	/**
	 * Initializes empty path
	 */
	public Path() {
		this.first = null;
		this.last = null;
	}

	/**
	 * Insert the given coordinates as a new step in the first position of the
	 * path
	 */
	public void insertFirst(int row, int col) {
		PathStep prevFirst = first;
		first = new PathStep(row, col, prevFirst, null);
		if (prevFirst != null) {
			prevFirst.setPrev(first);
		} else {
			last = first;
		}
	}

	/**
	 * Insert the given coordinates as a new step in the last position of the
	 * path
	 */
	public void insertLast(int row, int col) {
		PathStep prevLast = last;
		last = new PathStep(row, col, null, prevLast);
		if (prevLast != null) {
			prevLast.setNext(last);
		} else {
			first = last;
		}
	}

	/**
	 * Returns the coordinates of the first step in the path and removes it from
	 * the path. If the Path is empty, returns null.
	 */
	public int[] extractFirst() {
		if (first != null) {
			PathStep result = first;
			PathStep next = first.getNext();
			if (next != null) {
				next.setPrev(null);
			} else {
				last = null;
			}
			first = next;
			
			return new int[] { result.getRow(), result.getCol() };
		}

		return null;
	}

	/**
	 * Returns the coordinates of the last step in the path and removes it from
	 * the path. If the Path is empty, returns null.
	 */
	public int[] extractLast() {

		if (last != null) {
			PathStep result = last;
			PathStep prev = last.getPrev();
			
			if (prev != null) {
				prev.setNext(null);
			} else {
				first = null;
			}
			last = prev;
			
			return new int[] { result.getRow(), result.getCol() };
		}

		return null;
	}

	/**
	 * Returns a String based representation of the Path. If the Path is empty,
	 * returns an empty String ("") If the Path is not empty, returns an String
	 * with the format: (row1, col1), (row2, col2), ..., (rowN, colN)
	 */
	public String toString() {

		String sPath = "";

		// TO DO (Part 3)
		// Traverse a linked list concatenating the coordinates of each step.
		// Recommendation: use the toString method in the PathStep class to get
		// the
		// "(row, col)" String corresponding to the pair of coordinates of each
		// step.

		// Learning concepts:
		// traverse a linked list
		// Important: the Path must not be modified by the method!
		
		PathStep node = first;
		while (node != null) {
			sPath = sPath + "(" + node.getRow() + ", " + node.getCol() + "), ";
			node = node.getNext();
		}

		return sPath;
	}

}
