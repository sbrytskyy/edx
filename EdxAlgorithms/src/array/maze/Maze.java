package array.maze;

import java.util.Arrays;
import java.util.Vector;

/**
 * Class Maze represents a bidimensional maze to traverse and find a path
 * through
 * 
 * @author Raquel M. Crespo-Garcia <rcrespo@it.uc3m.es>
 */
public class Maze {

	private MazeStatus[][] maze;

	/**
	 * Creates an square Maze of given size
	 */
	public Maze(int size) {
		this(size, size);
	}

	/**
	 * Creates a bidimensional Maze of given height and size, with all positions
	 * initially open
	 */
	public Maze(int width, int height) {
		maze = new MazeStatus[width][height];
		for (MazeStatus[] row : maze) {
			Arrays.fill(row, MazeStatus.OPEN);
		}
	}

	/**
	 * Initializes a Maze from the given textual representation
	 */
	public Maze(String sMaze) {
		this.maze = stringToMaze(sMaze);
	}

	/**
	 * Returns the MazeStatus value corresponding to the given cell, specified
	 * by its row and column
	 */
	public MazeStatus getPosStatus(int row, int col) {
		return maze[row][col];
	}

	/**
	 * Sets the cell corresponding to the specified row and column to the given
	 * status value
	 */
	public void setPosStatus(int row, int col, MazeStatus newStatus) {
		maze[row][col] = newStatus;
	}

	/**
	 * Creates and returns an String with the text-based representation of the
	 * given Maze
	 */
	public String toString() {
		String s = "";

		for (MazeStatus[] row : maze) {
			for (int col = 0; col < row.length; col++) {
				s += row[col].text();
			}
			s += "\n";
		}

		return s;
	}

	/**
	 * Transforms a text-based Maze into a bidimensional array of positions with
	 * the corresponding status. It creates the array maze and assign to each of
	 * its cells the corresponding status value based on the given text
	 * representation.
	 */
	private MazeStatus[][] stringToMaze(String sMaze) {

		/*
		 * Recommendations:
		 * 
		 * Method split in class String can be useful for dividing the given
		 * String into a set of lines (one per row)
		 *
		 * Method toCharArray in class String can also be useful for
		 * transforming each line into an array of chars, one per cell
		 *
		 * Use the MazeStatus enumeration type for first checking the character
		 * corresponding to the cell status, and next assigning the
		 * corresponding status to the array cell
		 */

		String[] sa = sMaze.split("\n");
		maze = new MazeStatus[sa.length][sa[0].length()];

		for (int row = 0; row < sa.length; row++) {
			for (int col = 0; col < sa[row].length(); col++) {
				char ch = sa[row].charAt(col);
				maze[row][col] = MazeStatus.getMazeStatusForName(ch);
			}
		}

		return maze;

	}

	/**
	 * Calculates the destination position in the Maze given an starting
	 * position (row, col) and a Movement (mov)
	 * 
	 * @returns the coordinates of the next position, if its is a valid
	 *          position. Returns null if the destination position is outside
	 *          the limits of the array.
	 */
	public int[] getNeighbourCoords(int row, int col, Movement mov) {

		if (mov.equals(Movement.LEFT)) {
			if (col > 0) {
				return new int[] { row, --col };
			}
		} else if (mov.equals(Movement.RIGHT)) {
			if (col < maze[0].length - 1) {
				return new int[] { row, ++col };
			}
		} else if (mov.equals(Movement.DOWN)) {
			if (row < maze.length - 1) {
				return new int[] { ++row, col };
			}
		} else if (mov.equals(Movement.UP)) {
			if (row > 0) {
				return new int[] { --row, col };
			}
		}

		return null;
	}

	/**
	 * Changes the state of the maze positions following the given path. If the
	 * initial status of the position to visit is OPEN or VISITED (can step into
	 * the position), change it to VISITED. If the initial status of the
	 * position to visit is GOAL, does not change it so that the goal keeps
	 * displaying in the maze. If the initial status of the position to visit is
	 * OBSTACLE or the position is outside the limits of the Maze, it is an
	 * invalid move, stop and finish. Empties the received path as it traverses
	 * it.
	 * 
	 */
	public void followPath(Path path) {
		int[] pos;
		while ((pos = path.extractFirst()) != null ) {
			MazeStatus status = getPosStatus(pos[0], pos[1]);
			if (status.equals(MazeStatus.VISITED) || status.equals(MazeStatus.GOAL)) {
				// do nothing
			} else if (status.equals(MazeStatus.OPEN)) {
				setPosStatus(pos[0], pos[1], MazeStatus.VISITED);
			} else if (status.equals(MazeStatus.OBSTACLE)) {
				return;
			}
		}
	}
}
