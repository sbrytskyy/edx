package queue.circular;

/**
 * Universidad Carlos III de Madrid
 * Departamento de Ingenieria Telematica.
 * Int circular queue
 * 2017
 */

/**
  * Int Queue
  */
public class Queue {

	/** Number of element in the queue */
	private int numElements;
	private int capacity;

	/** Array to save elements **/
	private int elements[];

	/** Indice to last element */
	private int head;
	private int tail;

	// Add one position and calculates if we must return the first position array.
	// Module opertion (%) is very important for this.
	private int next(int pos) {
		return (pos + 1) % (capacity + 1);
	}

	/** Constructor to init the state object */
	Queue(int capacity) {
		numElements = 0;
		this.capacity = capacity;
		// We must create the array with an extra element to control the conditions empty and full
		elements = new int[capacity];
		tail = 0;
		head = 0;
	}

	/** Is empty the queue? */
	public boolean isEmpty() {
		return numElements == 0;
	}

	/** Is full the queue */
	public boolean isFull() {
		return numElements == capacity;
	}

	/** Insert an element in the queue 
	 * @throws Exception */
	public void enqueue(int element) throws Exception {
		if (isFull()) {
			throw new Exception("Queue is full");
		}
		elements[tail] = element;
		tail = (tail + 1) % capacity;
		numElements++;
	}

	/** Extract the element in the queue.
	 *  There isn't control error 
	 * @throws Exception */
	public int dequeue() throws Exception {
		if (isEmpty()) {
			throw new Exception("Queue is empty");
		}
		int result = elements[head];
		head = (head + 1) % capacity;
		numElements--;

		return result;
	}

	/** Returns the number of elements in the queue */
	public int numElements() {
		return numElements;
	}

	/** Print the elements in the queue*/
	public void print() {

		System.out.println("head: " + head + ", tail: " + tail + ", number of elements: " + numElements);

		for (int i = 0; i < numElements; i++) {
			System.out.print(elements[(head + i) % capacity]);
			if (i < numElements - 1) {
				System.out.print(", ");
			}
		}
		System.out.println();
	}

	public static void main(String args[]) throws Exception {
		// Five elements maximun in the queue
		Queue queue = new Queue(5);

		System.out.println("Is empty?: " + queue.isEmpty());
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		System.out.println("Is full?: " + queue.isFull());

		int e;

		e = queue.dequeue();
		e = queue.dequeue();

		queue.print();

		e = queue.dequeue();
		e = queue.dequeue();

		queue.print();

		e = queue.dequeue();

		queue.print();

		queue.enqueue(1);

		queue.print();

		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);

		queue.print();

		System.out.println("Is empty?: " + queue.isEmpty());
		System.out.println("Is full?: " + queue.isFull());

	} // main

} // Queue