package queue.palindrome;

public class Palindrome {

	public static boolean isPalindrome(String candidate) {
		String s = candidate.toLowerCase().replaceAll("[^a-zA-Z0-9]", "");
		System.out.println(candidate + ": " + s);

		LinkedStack<Character> chs = new LinkedStack<>();
		for (char ch : s.toCharArray()) {
			chs.push(ch);
		}

		for (char ch : s.toCharArray()) {
			if (ch != chs.pop()) {
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args) {

		System.out.println(Palindrome.isPalindrome("A man, a plan, a canal, Panama!"));

		System.out.println(Palindrome.isPalindrome("Was it a car or a cat I saw?"));

		System.out.println(Palindrome.isPalindrome("No 'x' in Nixon"));

		System.out.println(Palindrome.isPalindrome("Not a Palindrom"));

	}
}