package queue.linkedlist;

public class LinkedQueue<E> implements Queue<E> {
	// Attributes
	
	private Node<E> front;
	
	private int size;

	public LinkedQueue() {
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public int size() {
		return size;
	}

	public E front() {
		if (!isEmpty()) return front.getInfo();
		return null;
	}

	public void enqueue(E info) {
		Node<E> newNode = new Node<>(info);
		size++;
		
		if (front == null) {
			front = newNode;
			return;
		}
		
		Node<E> node = front;
		while (node.getNext() != null) {
			node = node.getNext();
		}
		node.setNext(newNode);
	}

	public E dequeue() {
		if (!isEmpty()) {
			E result = front.getInfo();
			front = front.getNext();
			size--;
			return result;
		}
		return null;
	}

	public static void main(String[] args) {
		LinkedQueue<Integer> q = new LinkedQueue<Integer>();

		q.enqueue(1);
		q.enqueue(2);
		q.enqueue(3);
		q.enqueue(4);
		q.enqueue(5);

		System.out.println("Size: " + q.size());

		Integer e = q.front();
		System.out.println("Size: " + q.size());
		System.out.println(e);

		e = q.dequeue();
		e = q.dequeue();
		System.out.println("Size: " + q.size());

		e = q.dequeue();
		e = q.dequeue();
		e = q.dequeue();
		System.out.println("Size: " + q.size() + " isEmpty: " + q.isEmpty());
	}
}
