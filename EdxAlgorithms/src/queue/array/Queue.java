package queue.array;

/**
 * Universidad Carlos III de Madrid
 * Departamento de Ingenieria Telematica.
 * Int queue
 * 2017
 */

/**
  * Int Queue
  */
public class Queue {

	/** Max num elements*/
	private int numElements;

	/** Array to save elements **/
	private int elements[];

	/** Indice to last element */
	private int last = 0;

	private int first = 0;
	private int size = 0;

	/** Constructor to init the state object */
	Queue(int numElements) {
		this.numElements = numElements;
		elements = new int[numElements];
	}

	/** Is empty the queue? */
	public boolean isEmpty() {
		return size == 0;
	}

	/** Is full the queue */
	public boolean isFull() {
		return size == numElements;
	}

	/** Insert an element in the queue 
	 * @throws Exception */
	public void enqueue(int element) throws Exception {
		if (isFull()) {
			throw new Exception("Queue is full");
		}
		elements[last] = element;
		last = (last + 1) % numElements;
		size++;
	}

	/** Extract the element in the queue.
	 *  There isn't control error 
	 * @throws Exception */
	public int dequeue() throws Exception {
		if (isEmpty()) {
			throw new Exception("Queue is empty");
		}
		int result = elements[first];
		first = (first + 1) % numElements;
		size--;

		return result;
	}

	/** Returns the number of elements in the queue */
	public int numElements() {
		return size;
	}

	/** Print the elements in the queue*/
	public void print() {
		System.out.println("head: " + first + ", tail: " + last + ", number of elements: " + size);
		
		for (int i = 0; i < size; i++) {
			System.out.print(elements[(first + i) % numElements]);
			if (i < size - 1) {
				System.out.print(", ");
			}
		}
		System.out.println();
	}

	public static void main(String args[]) throws Exception {
		// test the class
		System.out.println("Test the queue class");

		Queue q = new Queue(20);

		for (int i = 0; i < 20; i++) {
			q.enqueue(i);
		}

		for (int i = 0; i < 10; i++) {
			q.dequeue();
		}

		for (int i = 20; i < 30; i++) {
			q.enqueue(i);
		}

		q.print();

		Queue queue = new Queue(3);

		queue.enqueue(1);
		queue.enqueue(2);
		queue.print();

		int e = queue.dequeue();
		System.out.println(e);
		queue.print();

		queue.enqueue(3);
		queue.print();

		// queue.enqueue(4);
		e = queue.dequeue();
		System.out.println(e);
		queue.print();

		queue.enqueue(4);
		queue.print();

		e = queue.dequeue();
		e = queue.dequeue();
		queue.print();

		// e = queue.dequeue();

		queue.enqueue(1);
		queue.enqueue(2);
		queue.print();

	} // main

} // Queue
